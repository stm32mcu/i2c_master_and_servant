# I2C_Master_and_Servant

I2C1 is configured as I2C master, I2C3 as I2C slave. 
Both interfaces need to be connected.

The slave acts as it was a 256 byte serial I2C EEPROM.

The master accesss the "EEPROM" using **HAL_I2C_Mem_Read** and **HAL_I2C_Mem_Write**.

An interrupt driven design was chosen for the I2C slave using 
**HAL_I2C_EnableListen_IT** and a number of callbacks.